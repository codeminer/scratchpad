const { app, BrowserWindow, dialog } = require('electron');
const windowStateKeeper = require('electron-window-state');
const sessionStorage = require('electron-browser-storage');
const os = require('os');

let mainWin;

app.getPath('userData');


function createWindow() {
    let winState = windowStateKeeper({
        defaultWidth: 1000,
        defaultHeight: 800
    });

    let iconpath = os.type() != 'Windows_NT'? (__dirname + '/build/icon.icns') : (__dirname + '\\build\\icon.png');
    console.log(iconpath);

    mainWin = new BrowserWindow({
        width: 1000, 
        height: 700,
        x: winState.x,
        y: winState.y,
        minWidth: 300,
        minHeight: 150,
        webPreferences: {
            nodeIntegration: true
        },
        icon: iconpath
    });

    let wc = mainWin.webContents;

    mainWin.loadFile('index.html');

    winState.manage(mainWin);

    mainWin.on('closed', () => {
        mainWin = null;
    });
}

app.on('ready', createWindow);

app.on('browser-window-focus', function() {
    app.dock && app.dock.setBadge('');
})

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin') {
        app.quit();
    }
})

exports.selectDirectory = function () {
  return dialog.showOpenDialog(mainWin, {
    properties: ['openDirectory']
  })
}