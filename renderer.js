const electron = require('electron');
const remote = electron.remote;
const { dialog, nativeImage, Menu, Tray, app } = require('electron').remote;
const { getOrgsList, getCurrentWorkingDir, setCurrentWorkingDir, setCurrentWorkingOrg, getCurrentWorkingOrg, getLastOrgDeploy, getOrgByRef, enableAvailableSvnInf } = require("./src/scripts/sessionManager");
const { refreshOrgLst, getOrgInfo, deleteOrg, openOrg, renderOrgTable, pushToOrg, createOrg, updateLatestDeploy, openLastDeploy, showHide } = require("./src/scripts/orgManager");
const _ = require('underscore');
const os = require('os');
const spinner = require("./src/scripts/spinner");


let contextMenu;
let trayIcon;
let pushType = '';

function createTemplate(selectedOrg) {
  let org = getOrgByRef(selectedOrg);
  let wd = getCurrentWorkingDir();
  let orgs = getOrgsList();
  if(orgs && orgs.scratchOrgs) {
    let template = [{
      label: 'Open ' + (selectedOrg ? selectedOrg : '~ please select org'), type: 'normal', 
      enabled: selectedOrg ? true: false,
      click: function() {
        openOrg(selectedOrg);
      }
    }, {
      label: 'Copy ' + (selectedOrg ? selectedOrg + ' details' : '~ please select org'), type: 'normal', 
      enabled: selectedOrg ? true: false,
      click: function() {
        getOrgInfo(org.orgId, selectedOrg);
      }   
    }, {
      label: 'Deploy to ' + (selectedOrg ? selectedOrg : '~ please select org'), 
      type: 'normal', 
      enabled: wd && selectedOrg ? true: false,
      click: function(menuItem, browserWindow, event) {
        menuItem.enabled = false;
        pushToOrg(selectedOrg, pushType).then(() => {
            menuItem.enabled = true;
        }).catch(() => {
            menuItem.enabled = true;
        });
      },             
    }, {
      type: "separator"
    }];

    _.each(orgs.scratchOrgs, function(org) {
        let orgref = org.alias && org.alias != 'undefined' ? org.alias : org.username;
        let menuitem = {label: orgref, type: 'normal'};
        menuitem.click = function(menuItem, browserWindow, event) {
            setCurrentWorkingOrg(menuItem.label);
            contextMenu = Menu.buildFromTemplate(createTemplate(menuItem.label));
            trayIcon.setContextMenu(contextMenu)
        };
        if(selectedOrg == orgref) {
          menuitem.enabled = false;
        }
        template.push(menuitem);
    })
    template.push({type: "separator"});
    let folderDivider = os.type() != 'Windows_NT' ? '/' : '\\';
    let folder;
    if(wd) {
      let pos = wd.lastIndexOf(folderDivider);
      folder = wd.substring(pos + 1);
      console.log(folder);
    }
    template.push({
      label: 'Push from folder: ' + (folder ? folder: 'none'),
      enabled: false
    });
    template.push({
      label: 'Change deploy folder ...',
      click: function(menuItem, browserWindow, event) {
        openSelectWorkingDirectory();
      }
    });      
    template.push({type: "separator"});

    template.push({
      label: 'All',
      enabled: !!pushType,
      click: function(menuItem, browserWindow, event) {
        menuItem.enabled = false;
        
        _.each(
          _.filter(contextMenu.items, function(item) {
            return item.label == 'People and Problems' || item.label == 'Foundation'
          }),
          function(item) {
            item.enabled = true;
          }
        );

        setPushType('');
      }      
    });
    template.push({
      label: 'People and Problems',
      enabled: pushType != 'People & Problems',
      click: function(menuItem, browserWindow, event) {
        menuItem.enabled = false;

        _.each(
          _.filter(contextMenu.items, function(item) {
            return item.label == 'All' || item.label == 'Foundation'
          }),
          function(item) {
            item.enabled = true;
          }
        );

        setPushType('People & Problems');
      }      
    });
    template.push({
      label: 'Foundation',
      enabled: pushType != 'Foundation',
      click: function(menuItem, browserWindow, event) {
        menuItem.enabled = false;

        _.each(
          _.filter(contextMenu.items, function(item) {
            return item.label == 'People and Problems' || item.label == 'All'
          }),
          function(item) {
            item.enabled = true;
          }
        );

        setPushType('Foundation');
      }      
    });

    template.push({type: "separator"});

    template.push({role: "quit"});
    return template;
  }
}

function createTrayItem() {
  let iconpath = os.type() != 'Windows_NT'? (__dirname + '/src/imgs/icon.png') : (__dirname + '\\src\\imgs\\icon.png');
  console.log(iconpath);
  trayIcon = nativeImage.createFromPath(iconpath);
  trayIcon = trayIcon.resize({ width: 16, height: 16 });

  trayIcon = new Tray(trayIcon);

  let deployinfo = getLastOrgDeploy();
  let lastOrgDeployed;
  if(deployinfo) {
    lastOrgDeployed = deployinfo.org;
  }
  contextMenu = Menu.buildFromTemplate(createTemplate(lastOrgDeployed));
  trayIcon.setContextMenu(contextMenu)
}

function openSelectWorkingDirectory() {
    app.show();
    app.focus();
    document.getElementById('selected-directory').click();
}

function setPushType(type) {
  pushType = type;
}

document.getOrgInfo = getOrgInfo;

document.deleteOrg = function(username, orgid) {
  dialog.showMessageBox({
    type: 'warning',
    buttons: ['Delete', 'Cancel'],
    title: 'Delete Org',
    message: `Delete and remove scratch org ${username}`,
    cancelId: 1
}).then(result => {
    if(result.response == 0) {
      deleteOrg(username, orgid);
    }
})
};

document.openOrg = openOrg;

document.refreshList = refreshOrgLst;

document.createOrg = createOrg;

document.showHide = showHide;

document.closeModal = function(modalselector) {
  document.querySelector(modalselector).classList.toggle('slds-fade-in-open');
  document.querySelector(".slds-backdrop").classList.toggle('slds-backdrop_open');  
}

document.pushToOrg = function(org) {
  pushToOrg(org);
};

document.openLastDeploy = openLastDeploy;

//Init org table with saved data or send of request for org list
let orgs = getOrgsList();
if(orgs) {
    renderOrgTable(orgs.scratchOrgs);
    createTrayItem();
    enableAvailableSvnInf();
} else {
    console.log('NOOO SAVED SESSION ORG DATA');
    refreshOrgLst().then(() => {
      createTrayItem();
      enableAvailableSvnInf();
    });
}

let workingdirectory = getCurrentWorkingDir();
if(workingdirectory) {
  document.querySelector("#selected-directory").innerHTML = workingdirectory;
}

updateLatestDeploy();

document.getElementById('selected-directory').addEventListener('click', _ => {
  selectDirectory().then(result => {
    console.log(result);

    if(!result.canceled && result.filePaths && result.filePaths.length) {
        setCurrentWorkingDir(result.filePaths[0]);
        contextMenu = Menu.buildFromTemplate(createTemplate(getCurrentWorkingOrg()));
        trayIcon.setContextMenu(contextMenu)
        document.querySelector("#selected-directory").innerHTML = result.filePaths[0];
    }
  })
})

let selectDirectory = function () {
  return dialog.showOpenDialog(remote.getCurrentWindow(), {
    properties: ['openDirectory']
  })
}